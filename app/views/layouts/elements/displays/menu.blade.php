<div class="navbar bs-docs-nav" role="banner">

  <div class="container">
    <div class="navbar-header">
    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>

    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li><a href="#"><i class="icon-home"></i></a></li>
        <li><a href="#">Giới thiệu</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sản phẩm & dịch vụ <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#">My Account</a></li>
            <li><a href="#">View Cart</a></li>
            <li><a href="#">Checkout</a></li>
            <li><a href="#">Wish List</a></li>
            <li><a href="#">Order History</a></li>
            <li><a href="#">Edit Profile</a></li>
            <li><a href="#">Confirmation</a></li>
          </ul>
        </li>
        <li><a href="#">Khuyến mãi</a></li>
        <li><a href="#">Đặt hàng</a></li>
        <li><a href="#">Phản hồi</a></li>
        <li><a href="#">Liên hệ</a></li>
      </ul>
    </nav>
  </div>
</div>