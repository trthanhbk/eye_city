<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Trung Thanh Print</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  {{ HTML::style('asset/frontend/style/bootstrap.css') }}
  {{ HTML::style('asset/frontend/style/prettyPhoto.css') }}
  {{ HTML::style('asset/frontend/style/sidebar-nav.css') }}
  {{ HTML::style('asset/frontend/style/flexslider.css') }}
  {{ HTML::style('asset/frontend/style/font-awesome.css') }}
  {{ HTML::style('asset/frontend/style/style.css') }}
  {{ HTML::style('asset/frontend/style/red.css') }}

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  {{ HTML::script('asset/frontend/js/html5shim.js') }}
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ url('asset/frontend/img/favicon/favicon.png') }}">
</head>

<body>


<!-- Header starts -->
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <!-- Logo. Use class "color" to add color to the text. -->
        <div class="logo">
          <h1><a href="#">Trung Thanh <span class="color bold"> Print</span></a></h1>
          <p class="meta">online printing is fun!!!</p>
        </div>
      </div>
    </div>
  </div>
</header>