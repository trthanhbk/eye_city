<div>
    <h4 class="title text-center">Danh mục thiết kế và in ấn</h4>
    <nav>
        <ul id="nav">
            <li class="has_sub"><a href="#">Ấn phẩm văn phòng</a>
                <ul>
                    <li><a href="#">Danh thiếp</a></li>
                    <li><a href="#">Tiêu đề thư</a></li>
                    <li><a href="#">Phong bì</a></li>
                    <li><a href="#">Bìa đựng hồ sơ</a></li>
                </ul>
            </li>
            <li class="has_sub"><a href="#">Ấn phẩm quảng cáo</a>
                <ul>
                    <li><a href="#">Tờ gấp</a></li>
                    <li><a href="#">Tờ rơi</a></li>
                    <li><a href="#">Poster</a></li>
                    <li><a href="#">Catalogue</a></li>
                </ul>
            </li>
            <li class="has_sub"><a href="#">Lịch</a>
                <ul>
                    <li><a href="#">Lịch độc quyền</a></li>
                    <li><a href="#">Lịch in phôi</a></li>
                    <li><a href="#">Lịch lò xo 7 tờ</a></li>
                    <li><a href="#">Lịch nẹp thiếc</a></li>
                    <li><a href="#">Thiếp chúc mừng năm mới</a></li>
                    <li><a href="#">Túi</a></li>
                </ul>
            </li>
            <li class="has_sub"><a href="#">Nhãn mác, tem bảo hành</a>
                <ul>
                    <li><a href="#">Nhãn decal</a></li>
                    <li><a href="#">Tem bảo hành</a></li>
                </ul>
            </li>
            <li class="has_sub"><a href="#" >Ấn phẩm khác</a>
                <ul>
                    <li><a href="#">Phiếu bảo hành</a></li>
                    <li><a href="#">Thực đơn</a></li>
                    <li><a href="#">Biểu mẫu</a></li>
                    <li><a href="#">Thẻ nhựa</a></li>
                    <li><a href="#">Vé giữ xe</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</div>